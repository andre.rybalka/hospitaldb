package com.andriirybalka.hospital.business;

import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.andriirybalka.hospital.beans.HospitalData;
import com.andriirybalka.hospital.persistence.HospitalDAO;
import com.andriirybalka.hospital.persistence.HospitalDAOImpl;

/**
 * Business class that uses the persistence layer to retrieve records
 * 
 */

public class HospitalManager {
    
private final Logger log = LoggerFactory.getLogger(this.getClass()
			.getName());

	private HospitalDAO hospitalDAO;
	
	/**
	 * Constructor
	 */
	public HospitalManager() {
		super();
		hospitalDAO = new HospitalDAOImpl();		
	}

	/**
	 * This method retrieves all the records and returns them as a string so
	 * that the string can be displayed in the UI
	 * 
	 * @return String containing all the patient
	 */
	public String retrievePatient() {

		StringBuilder sb = new StringBuilder();

		ArrayList<HospitalData> data = null;
		try {
			data = hospitalDAO.findAll();
			for (HospitalData hd : data) {
				sb.append(hd.toString()).append("\n");
			}
		} catch (SQLException e) {
			log.error("Error retrieving records: ", e.getCause());
		}
		return sb.toString();
	}
}

