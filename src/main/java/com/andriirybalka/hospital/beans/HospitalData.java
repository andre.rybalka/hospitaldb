/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andriirybalka.hospital.beans;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.Date;

/**
 *
 * @author a_rybalk
 */
public class HospitalData {

    private int patientid;
    private String lastName;
    private String firstName;
    private String diagnosis;
    private LocalDateTime admissionDate;
    private LocalDateTime releaseDate;
    private ArrayList<Medication> meds;
    private ArrayList<Surgical> sergs;
    private ArrayList<Inpatient> inps;

    /**
     * Non-default constructor
     *
     * @param patientid
     * @param lastName
     * @param firstName
     * @param diagnosis
     * @param admissionDate
     * @param releaseDate
     */
    public HospitalData(final int patientid, final String lastName, final String firstName, final String diagnosis,
            final LocalDateTime admissionDate, final LocalDateTime releaseDate, final ArrayList<Medication> meds , final ArrayList<Surgical> sergs, final ArrayList <Inpatient> inps) {
        super();
        this.patientid = patientid;
        this.lastName = lastName;
        this.firstName = firstName;
        this.diagnosis = diagnosis;
        this.admissionDate = admissionDate;
        this.releaseDate = releaseDate;
        this.meds = meds;
        this.sergs = sergs;
        this.inps = inps;

    }

    /**
     * Default Constructor
     */
    public HospitalData() {
        super();
        this.patientid = -1;
        this.lastName = "";
        this.firstName = "";
        this.diagnosis = "";
        this.admissionDate = LocalDateTime.now();
        this.releaseDate = LocalDateTime.now();
    }

    public int getPatientid() {
        return patientid;
    }

    public void setPatientid(final int patientid) {
        this.patientid = patientid;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(final String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public LocalDateTime getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(final LocalDateTime addmissionDate) {
        this.admissionDate = admissionDate;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(final LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public String toString() {
        String s = "HospitalData{" + "patientid=" + patientid + "\n" + ", lastName=" + lastName + "\n" + ", firstName=" + firstName + "\n" + ", diagnosis=" + diagnosis + "\n" + ", admissionDate=" + admissionDate + "\n" + ", releaseDate=" + releaseDate + '}';
        return s;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.patientid;
        hash = 29 * hash + Objects.hashCode(this.lastName);
        hash = 29 * hash + Objects.hashCode(this.firstName);
        hash = 29 * hash + Objects.hashCode(this.diagnosis);
        hash = 29 * hash + Objects.hashCode(this.admissionDate);
        hash = 29 * hash + Objects.hashCode(this.releaseDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HospitalData other = (HospitalData) obj;
        if (this.patientid != other.patientid) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.diagnosis, other.diagnosis)) {
            return false;
        }
        if (!Objects.equals(this.admissionDate, other.admissionDate)) {
            return false;
        }
        if (!Objects.equals(this.releaseDate, other.releaseDate)) {
            return false;
        }
        return true;
    }
    private LocalDateTime convertToLocalDateTime(String dateTime) {

       DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
       LocalDateTime localDateTime = LocalDateTime.parse(dateTime, formatter);

       return localDateTime;
   }

}
