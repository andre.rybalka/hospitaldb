package com.andriirybalka.hospital.beans;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Objects;
import java.util.Date;

public class Medication {

    private int id;
    private int patientid;
    private LocalDateTime medicationDate;
    private String medName;
    private double unitcost;
    private int unit;

    public Medication(final int id, final int medicationid, final int patientid,
            final LocalDateTime medicationDate, final String medName,
            final double unitcost, final int unit) {
        super();
        this.id = id;
        this.patientid = patientid;
        this.medicationDate = medicationDate;
        this.medName = medName;
        this.unitcost = unitcost;
        this.unit = unit;

    }

    public Medication() {
        super();
        id = -1;
        patientid = 1;
        medicationDate = LocalDateTime.now();
        medName = "";
        unitcost = 1.0;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public int getPatientid() {
        return patientid;
    }

    public void setPatientid(final int patientid) {
        this.patientid = patientid;
    }

    public LocalDateTime getMedicationDate() {
        return medicationDate;
    }

    public void setMedicationDate(final LocalDateTime medicationDate) {
        this.medicationDate = medicationDate;
    }

    public String getMedName() {
        return medName;
    }

    public void setMedName(final String medName) {
        this.medName = medName;
    }

    public double getUnitcost() {
        return unitcost;
    }

    public void setUnitcost(final double unitcost) {
        this.unitcost = unitcost;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(final int unit) {
        this.unit = unit;
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.id;
        hash = 53 * hash + this.patientid;
        hash = 53 * hash + Objects.hashCode(this.medicationDate);
        hash = 53 * hash + Objects.hashCode(this.medName);
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.unitcost) ^ (Double.doubleToLongBits(this.unitcost) >>> 32));
        hash = 53 * hash + this.unit;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Medication other = (Medication) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.patientid != other.patientid) {
            return false;
        }
        if (Double.doubleToLongBits(this.unitcost) != Double.doubleToLongBits(other.unitcost)) {
            return false;
        }
        if (this.unit != other.unit) {
            return false;
        }
        if (!Objects.equals(this.medName, other.medName)) {
            return false;
        }
        if (!Objects.equals(this.medicationDate, other.medicationDate)) {
            return false;
        }
        return true;
    }

    

}
