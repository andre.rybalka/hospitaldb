/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andriirybalka.hospital.beans;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Objects;
import java.util.Date;

/*
 LocalDateTime ldt = timeStamp.toLocalDateTime();
  Timestamp ts = Timestamp.valueOf(dateTime);
 */
public class Surgical {

    private int id;
    private int patientid;
    private LocalDateTime dateofsurgery;
    private String surgery;
    private double roomfee;
    private double surgeonfee;
    private double suplies;

    public Surgical(final int id, final int patientid, final LocalDateTime dateofsurgery,
            final String surgery, final double roomfee, final double surgeonfee,
            final double suplies) {
        super();
        this.id = id;
        this.patientid = patientid;
        this.dateofsurgery = dateofsurgery;
        this.surgery = surgery;
        this.roomfee = roomfee;
        this.surgeonfee = surgeonfee;
    }
    public Surgical(){
        super();
        this.id = -1;
        this.patientid = -1;
        this.dateofsurgery = LocalDateTime.now();
        this.surgery ="";
        this.roomfee = 1.0;
        this.surgeonfee = 1.0;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public int getPatientid() {
        return patientid;
    }

    public void setPatientid(final int patientid) {
        this.patientid = patientid;
    }
    

    public LocalDateTime getDateofsurgery() {
        return dateofsurgery;
    }

    public void setDateofsurgery(final LocalDateTime dateofsurgery) {
        this.dateofsurgery = dateofsurgery;
    }
    
    

    public String getSurgery() {
        return surgery;
    }

    public void setSurgery(final String surgery) {
        this.surgery = surgery;
    }

    
    public double getRoomfee() {
        return roomfee;
    }

    public void setRoomfee(final double roomfee) {
        this.roomfee = roomfee;
    }
    
    

    public double getSurgeonfee() {
        return surgeonfee;
    }

    public void setSurgeonfee(final double surgeonfee) {
        this.surgeonfee = surgeonfee;
    }

    
    
    public double getSuplies() {
        return suplies;
    }

    public void setSuplies(final double suplies) {
        this.suplies = suplies;
    }

    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.patientid;
        hash = 29 * hash + Objects.hashCode(this.dateofsurgery);
        hash = 29 * hash + Objects.hashCode(this.surgery);
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.roomfee) ^ (Double.doubleToLongBits(this.roomfee) >>> 32));
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.surgeonfee) ^ (Double.doubleToLongBits(this.surgeonfee) >>> 32));
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.suplies) ^ (Double.doubleToLongBits(this.suplies) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Surgical other = (Surgical) obj;
        if (this.patientid != other.patientid) {
            return false;
        }
        if (Double.doubleToLongBits(this.surgeonfee) != Double.doubleToLongBits(other.surgeonfee)) {
            return false;
        }
        if (!Objects.equals(this.surgery, other.surgery)) {
            return false;
        }
        if (!Objects.equals(this.dateofsurgery, other.dateofsurgery)) {
            return false;
        }
        return true;
    }
    
    
    
}
