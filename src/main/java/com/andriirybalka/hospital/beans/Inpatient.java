package com.andriirybalka.hospital.beans;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.Date;

public class Inpatient {

    private int id;
    private int patientid;
    private LocalDateTime dateofstay;
    private String roomnumber;
    private double dailyrate;
    private double supplies;
    private double services;
    
public Inpatient(final int id, final int patientid, 
        final LocalDateTime dateofstay, final String roomnumber,
        final double dailyrate, final double supplies,
        final double services)
{
    super();
    this.id = id;
    this.patientid = patientid;
    this.dateofstay = dateofstay;
    this.roomnumber = roomnumber;
    this.dailyrate = dailyrate;
    this.supplies = supplies;
    this.services = services;
}
public Inpatient(){
    super();
    this.id = -1;
    this.patientid = -1;
    this.dateofstay = LocalDateTime.now();
    this.roomnumber = "";
    this.dailyrate = 1.0;
    this.supplies = 1.0;
    this.services = 1.0;
}

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }
    

    public int getPatientid() {
        return patientid;
    }

    public void setPatientid(final int patientid) {
        this.patientid = patientid;
    }
    

    public LocalDateTime getDateofstay() {
        return dateofstay;
    }

    public void setDateofstay(final LocalDateTime dateofstay) {
        this.dateofstay = dateofstay;
    }
    
    public String getRoomnumber() {
        return roomnumber;
    }

    public void setRoomnumber(final String roomnumber) {
        this.roomnumber = roomnumber;
    }
    

    public double getDailyrate() {
        return dailyrate;
    }

    public void setDailyrate(final double dailyrate) {
        this.dailyrate = dailyrate;
    }

    public double getSupplies() {
        return supplies;
    }

    public void setSupplies(final double supplies) {
        this.supplies = supplies;
    }
    

    public double getServices() {
        return services;
    }

    public void setServices(final double services) {
        this.services = services;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.id;
        hash = 41 * hash + this.patientid;
        hash = 41 * hash + Objects.hashCode(this.dateofstay);
        hash = 41 * hash + Objects.hashCode(this.roomnumber);
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.dailyrate) ^ (Double.doubleToLongBits(this.dailyrate) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.supplies) ^ (Double.doubleToLongBits(this.supplies) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.services) ^ (Double.doubleToLongBits(this.services) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Inpatient other = (Inpatient) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.patientid != other.patientid) {
            return false;
        }
        if (Double.doubleToLongBits(this.dailyrate) != Double.doubleToLongBits(other.dailyrate)) {
            return false;
        }
        if (Double.doubleToLongBits(this.supplies) != Double.doubleToLongBits(other.supplies)) {
            return false;
        }
        if (Double.doubleToLongBits(this.services) != Double.doubleToLongBits(other.services)) {
            return false;
        }
        if (!Objects.equals(this.roomnumber, other.roomnumber)) {
            return false;
        }
        if (!Objects.equals(this.dateofstay, other.dateofstay)) {
            return false;
        }
        return true;
    }

    

    
    
}
