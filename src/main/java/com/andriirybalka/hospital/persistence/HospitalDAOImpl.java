/**
 * This class provides the functionality to: <ul> <li>1) Open a database <li>2)
 * Retrieve all the records from a user query and return them as an arraylist
 * <li>3) Close the database </ul>
 * Added logging Changed read to 3 methods, findAll, findID and findDiet
 * Eliminated returning null referencespackage com.andriirybalka.persistence;
 *
 */
package com.andriirybalka.hospital.persistence;

import com.andriirybalka.hospital.beans.HospitalData;
import com.andriirybalka.hospital.beans.Inpatient;
import com.andriirybalka.hospital.beans.Medication;
import com.andriirybalka.hospital.beans.Surgical;

import java.sql.*;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.Date;

/**
 * This class implements the HospitalDAO interface
 *
 * Exceptions are possible whenever a JDBC object is used. When these exceptions
 * occur it should result in some message appearing to the user and possibly
 * some corrective action. To simplify this, all exceptions are thrown and not
 * caught here. The methods that you write to call any of these methods must
 * either use a try/catch or continue throwing the exception.
 *
 * Updated by creating a new method createHospitalData that used the resultSet
 * to create an object. Originally this code was repeated three times.
 *
 */
public class HospitalDAOImpl implements HospitalDAO {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    // This information should be coming from a Properties file
    private final String url = "jdbc:mysql://localhost:3306/hospitaldb";
    private final String user = "TheUser";
    private final String password = "pancake";

    /**
     * Retrieve all the records for the given table and returns the data as an
     * arraylist of HospitalData objects
     *
     * @return The arraylist of HospitalhData objects
     * @throws java.sql.SQLException
     */
    @Override
    public ArrayList<HospitalData> findAll() throws SQLException {

        ArrayList<HospitalData> parentRows = new ArrayList<>();
        String selectQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM PATIENT";
        // Using try with resources
        // This ensures that the objects in the parenthesis () will be closed
        // when block ends. In this case the Connection, PreparedStatement and
        // the ResultSet will all be closed.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);
                ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                parentRows.add(createHospitalData(resultSet));
            }
        }
        log.info("# of records found : " + parentRows.size());
        return parentRows;
    }

    /**
     * Retrieve one record from the given table based on the primary key
     *
     * @return The HospitalData object
     */
    @Override
    public HospitalData findPatientId(int id) throws SQLException {

        HospitalData hospitalData = new HospitalData();

        String selectQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = ?";

        // Using try with resources
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, id);
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    hospitalData = createHospitalData(resultSet);
                }
            }

            //
        }
        log.info("Found " + id + "?: " + (hospitalData != null));
        return hospitalData;
    }

    /**
     * Private method that creates an object of type HospitalData from the
     * current record in the ResultSet
     *
     * @param resultSet
     * @return
     * @throws SQLException
     */
    private HospitalData createHospitalData(ResultSet resultSet) throws SQLException {
        HospitalData hospitalData = new HospitalData();
        hospitalData.setLastName(resultSet.getString("LASTNAME"));
        hospitalData.setFirstName(resultSet.getString("FIRSTNAME"));
        hospitalData.setDiagnosis(resultSet.getString("DIAGNOSIS"));
        hospitalData.setAdmissionDate(resultSet.getTimestamp("ADMISSIONDATE").toLocalDateTime());
        hospitalData.setReleaseDate(resultSet.getTimestamp("RELEASEDATE").toLocalDateTime());
        log.info("HospitalData is created from ResultSet");
        return hospitalData;
    }

    /**
     * This method adds a HospitalData object as a record to the database. The
     * column list does not include ID as this is an auto increment value in the
     * table.
     *
     * @param hospitalData
     * @return The number of records created, should always be 1
     * @throws SQLException
     */
    @Override
    public int create(HospitalData hospitalData) throws SQLException {

        int result = 0;
        String createQuery = "INSERT INTO PATIENT (LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE) VALUES (?,?,?,?,?)";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(createQuery);) {
            ps.setString(1, hospitalData.getLastName());
            ps.setString(2, hospitalData.getFirstName());
            ps.setString(3, hospitalData.getDiagnosis());
            ps.setTimestamp(4, Timestamp.valueOf(hospitalData.getAdmissionDate()));
            ps.setTimestamp(5, Timestamp.valueOf(hospitalData.getAdmissionDate()));

            result = ps.executeUpdate();
        }
        log.info("# of records created : " + result);
        return result;
    }

    private Inpatient createInpatient(ResultSet resultSet) throws SQLException {
        Inpatient inpatient = new Inpatient();
        inpatient.setId(resultSet.getInt("ID"));
        inpatient.setPatientid(resultSet.getInt("PATIENTID"));
        inpatient.setDateofstay(resultSet.getTimestamp("DATEOFSTAY").toLocalDateTime());
        inpatient.setRoomnumber(resultSet.getString("ROOMNUMBER"));
        inpatient.setDailyrate(resultSet.getDouble("DAILYRATE"));
        inpatient.setSupplies(resultSet.getDouble("SUPPLIES"));
        inpatient.setServices(resultSet.getDouble("SERVICES"));
        log.info("Inpatient bean was created from ResultSet");
        return inpatient;
    }

    public Medication createMedication(ResultSet resultSet) throws SQLException {
        Medication medication = new Medication();
        medication.setId(resultSet.getInt("ID"));
        medication.setPatientid(resultSet.getInt("PATIENTID"));
        medication.setMedicationDate(resultSet.getTimestamp("DATEOFSTAY").toLocalDateTime());
        medication.setMedName(resultSet.getString("MED"));
        medication.setUnitcost(resultSet.getDouble("UNITCOST"));
        medication.setUnit(resultSet.getInt("UNITS"));
        return medication;
    }

    public Surgical createSurgical(ResultSet resultSet) throws SQLException {
        Surgical surgical = new Surgical();
        surgical.setId(resultSet.getInt("ID"));
        surgical.setPatientid(resultSet.getInt("PATIENTID"));
        surgical.setDateofsurgery(resultSet.getTimestamp("DATEOFSUGERY").toLocalDateTime());
        surgical.setSurgery(resultSet.getString("SURGERY"));
        surgical.setRoomfee(resultSet.getDouble("ROOMFEE"));
        surgical.setSurgeonfee(resultSet.getDouble("SURGEONFEE"));
        surgical.setSuplies(resultSet.getDouble("SUPPLIES"));

        return surgical;
    }

    public int findInpatient(int id, ArrayList<Inpatient> inpatient) throws SQLException {
        ArrayList<Inpatient> inpatientRows = new ArrayList<>();
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSTAY, ROOMNUMBER, "
                + "DAILYRATE, SUPPLIES, SERVICES "
                + "FROM INPATIENT WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, id);
            // second try-with-resources for ResultSet object
            try (ResultSet resultSet = pStatement.executeQuery();) {
                while (resultSet.next()) {
                    inpatient.add(createInpatient(resultSet));
                }
            }
        }
        log.info("# of Inpatient records found : " + inpatient.size());
        return inpatient.size();
    }

    public int delete(int id) throws SQLException {

        int result = 0;

        String deleteQuery = "DELETE FROM PATIENT WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, id);
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    @Override
    public ArrayList<HospitalData> findPatientName(String lastName) throws SQLException {

        ArrayList<HospitalData> rows = new ArrayList<>();

        String selectQuery = "SELECT LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE LASTNAME = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setString(1, lastName);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    rows.add(createHospitalData(resultSet));
                }
            }
        }
        log.info("# of records found : " + rows.size());
        return rows;
    }

    @Override
    public int update(HospitalData hospitalData) throws SQLException {

        int result = 0;

        String updateQuery = "UPDATE PATIENT SET LASTNAME=?, FIRSTNAME=?, DIAGNOSIS=?, ADMISSIONDATE=?, RELEASEDATE=? WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, hospitalData.getLastName());
            ps.setString(2, hospitalData.getFirstName());
            ps.setString(3, hospitalData.getDiagnosis());
            ps.setTimestamp(4, Timestamp.valueOf(hospitalData.getAdmissionDate()));
            ps.setTimestamp(5, Timestamp.valueOf(hospitalData.getReleaseDate()));

            result = ps.executeUpdate();
        }
        log.info("# of records updated : " + result);
        return result;
    }

    

    @Override
    public int createInpatient(Inpatient inpatient) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int createMedication(Medication medication) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int createSurgical(Surgical surgical) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Inpatient findInpatient(int patientid) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
