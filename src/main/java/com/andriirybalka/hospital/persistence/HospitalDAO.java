package com.andriirybalka.hospital.persistence;

import com.andriirybalka.hospital.beans.HospitalData;
import com.andriirybalka.hospital.beans.Inpatient;
import com.andriirybalka.hospital.beans.Medication;
import com.andriirybalka.hospital.beans.Surgical;

import java.sql.SQLException;
import java.util.ArrayList;

public interface HospitalDAO {

    // Create
    public int create(HospitalData hospitalData) throws SQLException;
    public int createInpatient(Inpatient inpatient) throws SQLException;
    public int createMedication(Medication medication) throws SQLException;
    public int createSurgical(Surgical surgical) throws SQLException;

    public ArrayList<HospitalData> findAll() throws SQLException;
    

    public HospitalData findPatientId(int id) throws SQLException;
    public Inpatient findInpatient(int patientid)throws SQLException;

    public ArrayList<HospitalData> findPatientName(String lastName) throws SQLException;//check name
    // Update

    public int update(HospitalData hospitalData) throws SQLException;

    // Delete

    public int delete(int patientid) throws SQLException;
    // update
    

}





/*

    @Override
    public int update(HospitalData fishData) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
