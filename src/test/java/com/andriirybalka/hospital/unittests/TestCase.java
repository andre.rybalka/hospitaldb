package com.andriirybalka.hospital.unittests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.andriirybalka.hospital.beans.HospitalData;
import com.andriirybalka.hospital.beans.Inpatient;
import com.andriirybalka.hospital.persistence.HospitalDAO;
import com.andriirybalka.hospital.persistence.HospitalDAOImpl;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Rule;

public class TestCase {

    // This information should be coming from a Properties file
    private final String url = "jdbc:mysql://localhost:3306/";
    private final String user = "dbadmin";
    private final String password = "qwerty";
    
    private LocalDateTime convertStringToTime(String dateTime) {

       DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
       LocalDateTime time = LocalDateTime.parse(dateTime, formatter);

       return time;
   }
    
     @Rule
    public MethodLogger methodLogger = new MethodLogger();
     
    @Test
    public void testFindAll() throws SQLException {
        HospitalDAO hd = new HospitalDAOImpl();
        List<HospitalData> lhd = hd.findAll();
        assertEquals("testFindAll: ", 5, lhd.size());
    }
    
    @Test
    public void testCreateHospitalData() throws SQLException {
        //HospitalDAO hd = new HospitalDAOImpl();
        HospitalData patient6 = new HospitalData();
        patient6.setLastName("Norris");
        patient6.setFirstName("Chuck");
        patient6.setDiagnosis("So cool");
        patient6.setAdmissionDate(convertStringToTime("1999-12-31 23:59:59"));
        patient6.setReleaseDate(convertStringToTime("1900-12-31 23:59:59"));
        
        HospitalDAO patient = new HospitalDAOImpl();
        patient.create(patient6);
        
         HospitalData patient7 = patient.findPatientId(patient6.getPatientid());
         assertEquals(patient6,patient7);
    }
    @Test
    public void testCreateInpatient() throws SQLException {
        Inpatient inpatient21 = new Inpatient();
        
        
        inpatient21.setDateofstay(convertStringToTime("2010-12-31 23:59:59"));
        inpatient21.setRoomnumber("B2");
        inpatient21.setDailyrate(300);
        inpatient21.setSupplies(100.01);
        inpatient21.setServices(10.00);

        HospitalDAO inpatient = new HospitalDAOImpl();
        inpatient.createInpatient(inpatient21);
        
        Inpatient inpatient22 = inpatient.findInpatient(inpatient21.getId());
        assertEquals(inpatient21,inpatient22);
        
    }
    
    
    
    
    @Before
    public void seedDatabase() {
        final String seedDataScript = loadAsString("hospitaldb.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password);) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }
    
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }
    
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<String>();
        try {
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }
    
    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
    
    
}
